help([[

Description
===========
Python is a programming language that lets you work more quickly and
 integrate your systems more effectively.


More information
================
 - Homepage: http://python.org/


Included extensions
===================
arff-2.1.0, bitstring-3.1.5, blist-1.3.6, Cython-0.27.3, dateutil-2.6.0,
deap-1.0.2, decorator-4.0.11, docopt-0.6.2, ecdsa-0.13, enum34-1.1.6,
funcsigs-1.0.2, hmmlearn-0.2.0, HTSeq-0.9.1, intervaltree-2.1.0,
intervaltree_bio-1.0.1, joblib-0.11, matplotlib-2.1.0, mock-2.0.0,
mpi4py-2.0.0, multiqc-1.3, netaddr-0.7.19, netifaces-0.10.5, networkx-2.0,
nose-1.3.7, numpy-1.13.3, pandas-0.21.0, paramiko-2.1.2, paycheck-1.0.2,
pbr-2.0.0, pip-9.0.1, pycrypto-2.6.1, pyparsing-2.2.0, pysam-0.13,
pytz-2017.2, setuptools-33.1.1, six-1.10.0, virtualenv-15.1.0
]])

whatis([[Description: 
 Python is a programming language that lets you work more quickly and
 integrate your systems more effectively.
]])
whatis([[Homepage: http://python.org/]])
whatis([[Extensions: arff-2.1.0, bitstring-3.1.5, blist-1.3.6, Cython-0.27.3, dateutil-2.6.0, deap-1.0.2, decorator-4.0.11, docopt-0.6.2, ecdsa-0.13, enum34-1.1.6, funcsigs-1.0.2, hmmlearn-0.2.0, HTSeq-0.9.1, intervaltree-2.1.0, intervaltree_bio-1.0.1, joblib-0.11, matplotlib-2.1.0, mock-2.0.0, mpi4py-2.0.0, multiqc-1.3, netaddr-0.7.19, netifaces-0.10.5, networkx-2.0, nose-1.3.7, numpy-1.13.3, pandas-0.21.0, paramiko-2.1.2, paycheck-1.0.2, pbr-2.0.0, pip-9.0.1, pycrypto-2.6.1, pyparsing-2.2.0, pysam-0.13, pytz-2017.2, setuptools-33.1.1, six-1.10.0, virtualenv-15.1.0]])

local root = "/applis/PSMN/debian9/software/Core/Python/2.7.13"

load("GCC/7.2.0")

conflict("Python")

prepend_path("PYTHONPATH", pathJoin(root, "lib/python2.7/site-packages"))
prepend_path("CPATH", pathJoin(root, "include"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("MANPATH", pathJoin(root, "share/man"))
prepend_path("PATH", pathJoin(root, "bin"))
prepend_path("PKG_CONFIG_PATH", pathJoin(root, "lib/pkgconfig"))
setenv("EBROOTPYTHON", root)
setenv("EBVERSIONPYTHON", "2.7.13")
setenv("EBDEVELPYTHON", pathJoin(root, "easybuild/Core-Python-2.7.13-easybuild-devel"))

-- Built with EasyBuild version 3.4.0
setenv("EBEXTSLISTPYTHON", "setuptools-33.1.1,pip-9.0.1,nose-1.3.7,blist-1.3.6,mpi4py-2.0.0,paycheck-1.0.2,pbr-2.0.0,six-1.10.0,dateutil-2.6.0,deap-1.0.2,decorator-4.0.11,arff-2.1.0,pycrypto-2.6.1,ecdsa-0.13,paramiko-2.1.2,pyparsing-2.2.0,netifaces-0.10.5,netaddr-0.7.19,funcsigs-1.0.2,mock-2.0.0,pytz-2017.2,pandas-0.21.0,enum34-1.1.6,bitstring-3.1.5,virtualenv-15.1.0,docopt-0.6.2,joblib-0.11,multiqc-1.3,numpy-1.13.3,Cython-0.27.3,networkx-2.0,matplotlib-2.1.0,HTSeq-0.9.1,intervaltree-2.1.0,intervaltree_bio-1.0.1,pysam-0.13,hmmlearn-0.2.0")

-- PSMN: : 2.6.6.lua 2202 2018-03-30 09:00:33Z cpetit $
