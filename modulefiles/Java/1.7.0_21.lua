help([[

Description
===========
Java Platform, Standard Edition (Java SE) lets you develop and deploy
 Java applications on desktops and servers.


More information
================
 - Homepage: http://java.com/
]])

whatis([[Description: Java Platform, Standard Edition (Java SE) lets you develop and deploy
 Java applications on desktops and servers.]])
whatis([[Homepage: http://java.com/]])

local root = "/applis/PSMN/debian9/software/Core/Java/1.7.0_21"

conflict("Java")

prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("MANPATH", pathJoin(root, "man"))
prepend_path("PATH", pathJoin(root, "bin"))
setenv("EBROOTJAVA", root)
setenv("EBVERSIONJAVA", "1.7.0_21")
setenv("EBDEVELJAVA", pathJoin(root, "easybuild/Core-Java-1.7.0_21-easybuild-devel"))

prepend_path("PATH", root)
setenv("JAVA_HOME", "/applis/PSMN/debian9/software/Core/Java/1.7.0_21")
-- Built with EasyBuild version 3.4.0

-- PSMN: : 2.6.6.lua 2202 2018-03-30 09:00:33Z cpetit $
