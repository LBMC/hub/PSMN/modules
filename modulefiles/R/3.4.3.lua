help([[

Description
===========
R is a free software environment for statistical computing and graphics.


More information
================
 - Homepage: http://www.r-project.org/


Included extensions
===================
abind-1.4-5, ade4-1.7-10, b-a, codetools-0.2-14, colorspace-1.3-2, d-a,
data.table-1.10.4-3, dichromat-2.0-0, digest-0.6.14, evaluate-0.10.1,
foreach-1.4.4, g-r, g-r, g-r, geometry-0.3-6, gtable-0.2.0, iterators-1.0.9,
kernlab-0.9-25, labeling-0.3, lattice-0.20-35, latticeExtra-0.6-28, m-e,
magic-1.5-6, magrittr-1.5, markdown-0.8, MASS-7.3-48, Matrix-1.2-12,
mclust-5.4, mime-0.5, munsell-0.4.3, plyr-1.8.4, png-0.1-7, R6-2.2.2,
RColorBrewer-1.1-2, Rcpp-0.12.14, reshape2-1.4.3, s-p, s-t, s-t,
scatterplot3d-0.3-40, stringi-1.1.6, stringr-1.2.0, t-o, u-t, xtable-1.8-2
]])

whatis([[Description: R is a free software environment for statistical computing and graphics.]])
whatis([[Homepage: http://www.r-project.org/]])
whatis([[Extensions: abind-1.4-5, ade4-1.7-10, b-a, codetools-0.2-14, colorspace-1.3-2, d-a, data.table-1.10.4-3, dichromat-2.0-0, digest-0.6.14, evaluate-0.10.1, foreach-1.4.4, g-r, g-r, g-r, geometry-0.3-6, gtable-0.2.0, iterators-1.0.9, kernlab-0.9-25, labeling-0.3, lattice-0.20-35, latticeExtra-0.6-28, m-e, magic-1.5-6, magrittr-1.5, markdown-0.8, MASS-7.3-48, Matrix-1.2-12, mclust-5.4, mime-0.5, munsell-0.4.3, plyr-1.8.4, png-0.1-7, R6-2.2.2, RColorBrewer-1.1-2, Rcpp-0.12.14, reshape2-1.4.3, s-p, s-t, s-t, scatterplot3d-0.3-40, stringi-1.1.6, stringr-1.2.0, t-o, u-t, xtable-1.8-2]])

local root = "/applis/PSMN/debian9/software/Core/R/3.4.3"

conflict("R")

load("GCC/7.2.0")

load("Java/1.8.0_151")

prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib/R/lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib"))
prepend_path("LIBRARY_PATH", pathJoin(root, "lib/R/lib"))
prepend_path("MANPATH", pathJoin(root, "share/man"))
prepend_path("PATH", pathJoin(root, "bin"))
prepend_path("PKG_CONFIG_PATH", pathJoin(root, "lib/pkgconfig"))
setenv("EBROOTR", root)
setenv("EBVERSIONR", "3.4.3")
setenv("EBDEVELR", pathJoin(root, "easybuild/Core-R-3.4.3-easybuild-devel"))

-- Built with EasyBuild version 3.4.0
setenv("EBEXTSLISTR", "b-a,d-a,g-r,g-r,g-r,m-e,s-p,s-t,s-t,t-o,u-t,MASS-7.3-48,ade4-1.7-10,abind-1.4-5,magic-1.5-6,geometry-0.3-6,kernlab-0.9-25,mime-0.5,markdown-0.8,mclust-5.4,digest-0.6.14,iterators-1.0.9,stringi-1.1.6,magrittr-1.5,stringr-1.2.0,evaluate-0.10.1,scatterplot3d-0.3-40,lattice-0.20-35,RColorBrewer-1.1-2,latticeExtra-0.6-28,Matrix-1.2-12,png-0.1-7,Rcpp-0.12.14,plyr-1.8.4,colorspace-1.3-2,codetools-0.2-14,foreach-1.4.4,xtable-1.8-2,dichromat-2.0-0,reshape2-1.4.3,gtable-0.2.0,munsell-0.4.3,labeling-0.3,data.table-1.10.4-3,R6-2.2.2")

-- PSMN: : 2.6.6.lua 2202 2018-03-30 09:00:33Z cpetit $
