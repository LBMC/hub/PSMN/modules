#!/bin/bash
# Copyright laurent modolo for the LBMC UMR 5239 ©.
# contributor(s) : laurent modolo (2017)
#
# laurent.modolo@ens-lyon.fr
#
# This software is a computer program whose purpose is to manage dated file
# names in complience with the bioinformatic good practices used in the LBMC.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

source ../build_env.sh

SAMTOOLS_VERSION=1.5
curl -k -L https://sourceforge.net/projects/samtools/files/samtools/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2/download -o samtools-${SAMTOOLS_VERSION}.tar.bz2
tar xvf samtools-${SAMTOOLS_VERSION}.tar.bz2
rm -f samtools-${SAMTOOLS_VERSION}.tar.bz2
mv samtools-${SAMTOOLS_VERSION} ${SAMTOOLS_VERSION}
cd ${SAMTOOLS_VERSION}
./configure
make
mkdir -p bin share/man share/man1 misc/
prefix=$(pwd)
exec_prefix=$prefix
bindir=$exec_prefix/bin
datarootdir=$prefix/share
mandir=$datarootdir/man
man1dir=$mandir/man1
MKDIR_P="mkdir -p"
INSTALL="install -p"
INSTALL_DATA="$INSTALL -m 644"
INSTALL_DIR="$MKDIR_P -m 755"
INSTALL_MAN=$INSTALL_DATA
INSTALL_PROGRAM=$INSTALL
INSTALL_SCRIPT=$INSTALL_PROGRAM
PROGRAMS=samtools
MISC_PROGRAMS=" \
misc/ace2sam misc/maq2sam-long misc/maq2sam-short \
misc/md5fa misc/md5sum-lite misc/wgsim"
MISC_SCRIPTS=" \
misc/blast2sam.pl misc/bowtie2sam.pl misc/export2sam.pl \
misc/interpolate_sam.pl misc/novo2sam.pl \
misc/plot-bamstats misc/psl2sam.pl \
misc/sam2vcf.pl misc/samtools.pl misc/seq_cache_populate.pl \
misc/soap2sam.pl \
misc/varfilter.py misc/wgsim_eval.pl misc/zoom2sam.pl"
$($INSTALL_DIR $bindir $misc_bindir $man1dir)
$($INSTALL_PROGRAM $PROGRAMS $bindir)
$($INSTALL_PROGRAM $MISC_PROGRAMS $misc_bindir)
$($INSTALL_SCRIPT $MISC_SCRIPTS $misc_bindir)
$($INSTALL_MAN samtools.1 misc/wgsim.1 $man1dir)
